import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.lloseng.ocsf.server.AbstractServer;
import com.lloseng.ocsf.server.ConnectionToClient;

public class ChatServer extends AbstractServer {

	private List<ConnectionToClient> onlineClients;

	private enum State {
		START, LOGIN, STANDBY, MESSAGING;
	}

	public ChatServer(int port) {
		super(port);
		onlineClients = new ArrayList<ConnectionToClient>();
	}

	@Override
	protected void clientConnected(ConnectionToClient client) {
		super.clientConnected(client);
		onlineClients.add(client);
		client.setInfo("state", State.START);
	}

	@Override
	protected synchronized void clientDisconnected(ConnectionToClient client) {
		super.clientDisconnected(client);
		onlineClients.remove(client);
		client.setInfo("state", State.START);
		if (client.getInfo("username")!= null) {
			sendToAllClients("System: " + client.getInfo("username").toString() + " has disconnected.");
		}
	}

	@Override
	protected void handleMessageFromClient(Object msg, ConnectionToClient client) {

		String message = msg.toString();

		if (message.equalsIgnoreCase("logout")
				&& !(((State) client.getInfo("state")) == State.START)
				&& !(((State) client.getInfo("state")) == State.LOGIN)) {
			sendToAllClients("Notice: " + client.getInfo("username").toString()
					+ " have logged out.");
			sendToClient("System: Goodbye, please re-login to continue", client);
			client.setInfo("state", State.LOGIN);
		}

		switch ((State) client.getInfo("state")) {
		case START:
			sendToClient("System: Connection established, hello.", client);
			if (client.getInfo("username") == null
					|| isClientOnline(client.getInfo("username").toString())) {
				client.setInfo("state", State.LOGIN);
				sendToClient("System: Please login", client);
			} else {
				client.setInfo("state", State.STANDBY);
				sendToClient("System: Welcome, " + client.getInfo("username"), client);
				sendToAllClients("Notice: " + client.getInfo("username")
						+ "have logged in.");
				sendToClient("System: Message to? ", client);
			}
			break;
		case LOGIN:
			if (message.matches("login \\w+")
					&& !isClientOnline(message.substring(6).trim())) {
				client.setInfo("state", State.STANDBY);
				client.setInfo("username", message.substring(6).trim());
				sendToClient("System: Welcome, " + client.getInfo("username"), client);
				sendToAllClients("Notice: " + client.getInfo("username")
						+ " have logged in.");
				sendToClient("System: Message to? ", client);
			} else if (message.matches("login \\w+") && isClientOnline(message.substring(6).trim())) {
				sendToClient("SysErr: Username already exist, try another", client);
			} else {
				sendToClient("System: Please login using \"login [username]\"", client);
			}
			break;
		case STANDBY:
			if (isClientOnline(message)) {
				client.setInfo("state", State.MESSAGING);
				sendToClient("System: User online, now chatting with " + message,
						client);
				sendToClient("System: To change chat partner, use \"change;\"", client);
				sendToClient("System: To see online users, use \"find;\"", client);
				client.setInfo("talkingwith", message);
			} else {
				sendToClient("SysErr: User offline or does not exist!", client);
				sendToClient(
						"System: Please enter username without any spaces or quotes.",
						client);
			}
			break;
		case MESSAGING:
			if (message.matches("change\\;")) {
				client.setInfo("state", State.STANDBY);
				sendToClient("System: Change chat partner.", client);
				sendToClient("System: Message to?", client);
			} else if (message.matches("find\\;")) {
				sendToClient(getOnlineClientsList(), client);
			} else if (!isClientOnline(client.getInfo("talkingwith").toString())) {
				client.setInfo("state", State.STANDBY);
				sendToClient(
						"System: User has gone offline! Talk with someone else. Message to?",
						client);
				sendToClient("System: Message to?", client);
			} else {
				sendToClient(client.getInfo("username").toString() + " says: " + message,
						getOnlineClient(client.getInfo("talkingwith")
								.toString()));
			}
			break;
		}
	}

	private String getOnlineClientsList() {
		String returnCache = "List of users online\n";
		for (ConnectionToClient ctc : onlineClients) {
			if (ctc.getInfo("username") != null) {
				returnCache += ">>" + ctc.getInfo("username").toString() + "\n";
			}
		}
		return returnCache;
	}

	private ConnectionToClient getOnlineClient(String clientName) {
		for (ConnectionToClient ctc : onlineClients) {
			if (ctc.getInfo("username") != null
					&& ctc.getInfo("username").toString().equals(clientName)) {
				return ctc;
			}
		}
		return null;
	}

	private boolean isClientOnline(String clientName) {
		for (ConnectionToClient ctc : onlineClients) {
			if (ctc.getInfo("username") != null
					&& ctc.getInfo("username").toString().equals(clientName)) {
				return true;
			}
		}
		return false;
	}

	private void sendToClient(String string, ConnectionToClient client) {
		try {
			client.sendToClient(string);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws IOException {
		ChatServer chatServer = new ChatServer(5000);
		chatServer.listen();
	}
}
