import java.io.IOException;
import java.util.Scanner;

import com.lloseng.ocsf.client.AbstractClient;

public class Client extends AbstractClient {

	public Client(String host, int port) {
		super(host, port);
	}

	@Override
	protected void handleMessageFromServer(Object msg) {
		System.out.println("> " + msg);

	}

	public static void main(String[] args) throws IOException {
		String server = "127.0.0.1";
		int port = 5000;
		@SuppressWarnings("resource")
		final Scanner scan = new Scanner(System.in);
		Client client = new Client(server, port);
		client.openConnection();
		System.out.println("Connected to server: " + server + ":" + port);
		String input = "";
		while (!input.equalsIgnoreCase("Quit")) {
			input = scan.nextLine();
			client.sendToServer(input);
		}
	}
}
